# shopec-b2c

#### 项目介绍
该商城是一个综合性的B2C平台，类似京东商城。用户可以在商城浏览商品、下订单，以及参加各种活动。该商城采用Spring Boot+MyBatis框构，为了提高系统的性能使用Ehcache做系统缓存，搜索功能使用Elasticsearch做搜索引擎。

## 技术选型

1、后端

* 核心框架：Spring Boot 2.0.3.RELEASE
* 安全框架：Apache Shiro 1.4.0
* 视图框架：Spring MVC 5.0.6
* 搜索框架：Elasticsearch 5.6.10
* 任务调度：Spring + Quartz 2.2.3
* 持久层框架：MyBatis 3.4.6 + Mybatis-plus 2.3
* 数据库连接池：Alibaba Druid 1.1.10
* 缓存框架：Ehcache 2.6 + Redis 2.9.0
* 日志管理：SLF4J 1.7 + Log4j2 2.7
* 工具类：Apache Commons、Jackson 2.9.6、fastjson 1.2.6
    

2、前端
* JS框架：Jquery
* 表格插件：Bootstrap Table
* 表单验证插件：Jquery.Validate
* 日期选择插件：Datepicker for Bootstrap
* 数据图表：Echarts
* 后台管理系统模版：AdminLTE

## 快速体验

> 运行项目配置说明

1.后台管理系统

```
1、具备运行环境：JDK1.8+、Maven3.0+、MySql5.6+

2、根据 src\main\resources\application-dev.yml 配置数据库

3、导入数据库 shopecb2c.sql

4、安装Elasticsearch，application.yml 中修改配置地址

5、选中ApplicationB2C.java右击 -> Run As -> Java Application

6、后台管理系统账号：admin 密码：123456

## 特别说明

详细操作手册请与我们联系