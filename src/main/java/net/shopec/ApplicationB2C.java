package net.shopec;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApplicationB2C {

	public static void main(String[] args) {
		SpringApplication.run(ApplicationB2C.class, args);
	}
}
